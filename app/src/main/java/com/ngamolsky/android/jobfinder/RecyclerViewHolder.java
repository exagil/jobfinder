package com.ngamolsky.android.jobfinder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    TextView mRole, mEmployer, mLocation;

    public RecyclerViewHolder(View itemView) {
        super(itemView);

        mRole = (TextView) itemView.findViewById(R.id.list_title);
        mEmployer = (TextView) itemView.findViewById(R.id.list_desc);
        mLocation = (TextView) itemView.findViewById(R.id.list_desc);


    }
}